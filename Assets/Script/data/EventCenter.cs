using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Game/EventCenter", fileName = "EventCenter")]
public class EventCenter : ScriptableObject
{

    public UnityAction<string> debugEvent;

}
