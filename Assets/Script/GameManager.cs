using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameData gd;
    private EventCenter ec;

    private void Awake()
    {
        gd = SOLoader.get<GameData>();
        ec = SOLoader.get<EventCenter>();
    }

    private void Start()
    {
        gameObject.AddComponent<InputManager>();
        gameObject.AddComponent<UIManager>();
    }
}
