using System.Collections.Generic;
using UnityEngine;

public class SOLoader
{

    private static readonly Dictionary<System.Type, Object> map = new();

    public static T get<T>() where T : Object
    {
        var t = typeof(T);
        if (!map.TryGetValue(t, out var obj))
        {
            obj = Resources.Load($"SO/{t}");
            map.Add(t, obj);
        }
        return (T)obj;
    }

}
